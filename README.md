# stocknflow package

>`stocknflow` package displays two function for making maps with Python.  
> - One function allows you to **map stock values** as proportional circles.  
> - The another one allows you to **map flows** between two geographical entities as proportionnal arrows.

See Vignettes to know how to use them.

